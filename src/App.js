import React, { Component } from 'react';
import Content from './components/Content';
import Sidebar from './components/Sidebar';
import './resources/styles/App.css';

class App extends Component {
  render() {
    return (
      <div className="App card">
        <Content />
        <Sidebar />
      </div>
    );
  }
}

export default App;
