import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getSelectedCompetitorsIds } from '../selectors/competitors.selector';
import { updateCompetitors } from '../actions';


class Sidebar extends Component {
  
  approve = () => {
    this.props.updateCompetitors(1);
  }
  
  reprobate = () => {
    this.props.updateCompetitors(0);
  }
  
  render() {
    return(
      <div className="Sidebar">
        <div className="title">
          <h3 className="m-a-0">Cantidad seleccionados: {this.props.count} </h3>
        </div>
        <div className="sidebar-content">
          { this.props.count > 0 && (
            <div>
              <button onClick={this.approve} className="btn btn-success m-b-md">Aprobar</button>
              <button onClick={this.reprobate} className="btn btn-danger">Rechazar</button>
            </div>
          )}
          
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  const competitors = getSelectedCompetitorsIds(state);
  return {
    count: competitors.length,
  }
};

export default connect(mapStateToProps, {updateCompetitors})(Sidebar);
