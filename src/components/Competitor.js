import React, { Component } from 'react';

const Competitor = ({ id, avatar, name, bio, selected, onSelect, approved }) => {
  return (
    <div className={selected ? 'competitor selected' : 'competitor'} onClick={() => onSelect(id, selected)}>
      <div className="image">
        <img src={avatar} alt="avatar"/>
      </div>
      <div className="content">
        <div className="header">
          <h4 className="name m-a-0">{name}</h4>
          <div className={approved ? 'success' : 'danger'}>{ approved ? 'Aprobado' : 'Reprobado'}</div>
        </div>
        <p className="bio">{bio}</p>
      </div>
    </div>
  );
}

export default Competitor;