import React, { Component } from 'react';
import Competitor from './Competitor';
import { connect } from 'react-redux';
import { getCompetitorsSelected } from '../selectors/competitors.selector';
import { fetchCompetitors, selectCompetitors, unselectCompetitors } from '../actions';

class Content extends Component {
  
  componentDidMount() {
    this.props.fetchCompetitors();
  }
  
  onSelect = (id, selected) => {
    if (selected) {
      this.props.unselectCompetitors(id);
    } else {
      this.props.selectCompetitors(id);
    }
  }
  
  renderCompetitors = () => {
    const { competitors } = this.props;
    return competitors.map((competitor, i) => {
      const { id, avatar, name, bio, selected, approved } = competitor;
      return (
        <Competitor
          onSelect={this.onSelect}
          key={i}
          approved={approved}
          id={id}
          name={name}
          selected={selected}
          bio={bio} 
          avatar={avatar}
        />
      );
    });
  }
  render() {
    
    const quantity = this.props.competitors.length;
    
    return(
      <div className="Content">
        <div className="content-tittle">
          <h3 className="m-a-0">Mostrando { quantity } participantes</h3>
        </div>
        <div className="competitors">
          {this.renderCompetitors()}
        </div>
      </div>
    );
  }
  
};

const mapStateToProps = state => ({
  competitors: getCompetitorsSelected(state),
})

export default connect(mapStateToProps, { fetchCompetitors, selectCompetitors, unselectCompetitors})(Content);
