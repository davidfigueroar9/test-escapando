import { combineReducers } from 'redux';
import competitorsReducer from './competitorsReducer';
import selectedCompetitorsReducer from './selectedCompetitorsReducer';

const rootReducer = combineReducers({
  competitors: competitorsReducer,
  selectedCompetitors: selectedCompetitorsReducer,
});

export default rootReducer;