import { FETCH_COMPETITORS, UPDATE_COMPETITOR } from '../actions';

export default function (state = [], action) {
  switch (action.type) {
    case FETCH_COMPETITORS:
      return action.payload;
    case UPDATE_COMPETITOR:
      return state.map(competitor => {
        if (competitor.id === action.payload.id) {
          return action.payload;
        }
        return competitor;
      });
    default:
      return state;
  }
}
