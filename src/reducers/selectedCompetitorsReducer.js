import { SELECT_COMPETITOR, UNSELECT_COMPETITOR } from '../actions';

export default function (state = [], action) {
  switch (action.type) {
    case SELECT_COMPETITOR:
      return [...state, action.payload];
    case UNSELECT_COMPETITOR:
        return state.filter(x => x !== action.payload);
    default:
      return state;
  }
}
