import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import axios from 'axios';
import thunk from 'redux-thunk';
import registerServiceWorker from './registerServiceWorker';
import App from './App';
import reducers from './reducers';

import './resources/styles/index.css';

const axiosIntance = axios.create({
  baseURL: 'http://54.213.136.85:3001/'
});

const store = createStore(
  reducers,
  {},
  applyMiddleware(thunk.withExtraArgument(axiosIntance))
);

const AppProvider = () => {
  return(
    <Provider store={store}>
      <App />
    </Provider>
  );
}

ReactDOM.render(<AppProvider />, document.getElementById('root'));
registerServiceWorker();
