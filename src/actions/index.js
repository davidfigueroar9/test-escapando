export const FETCH_COMPETITORS = 'fetch_competitors';
export const fetchCompetitors = () => async (dispatch, getState, api) => {
  const res = await api.get('/competitors');
  dispatch({
    type: FETCH_COMPETITORS,
    payload: res.data
  });
};

export const SELECT_COMPETITOR = 'select_competitor';
export const selectCompetitors = (id) => (dispatch) => {
  dispatch({
    type: SELECT_COMPETITOR,
    payload: id
  });
};

export const UNSELECT_COMPETITOR = 'unselect_competitor';
export const unselectCompetitors = (id) => (dispatch) => {
  dispatch({
    type: UNSELECT_COMPETITOR,
    payload: id
  });
};

export const UPDATE_COMPETITOR = 'update_competitor';

export const updateCompetitors = (approved) => (dispatch, getState, api) => {
  const { selectedCompetitors } = getState();
  selectedCompetitors.map(async id => {
    const response = await api.patch('/competitors/' + id, {
      approved
    });
    dispatch({
      type: UPDATE_COMPETITOR,
      payload: response.data
    });
  });
};

