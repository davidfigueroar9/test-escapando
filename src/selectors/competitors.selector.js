import { createSelector } from 'reselect';
import _ from 'lodash';

const competitors = state => state.competitors;
const selectedCompetitors = state => state.selectedCompetitors;

export const getCompetitors = createSelector(
  competitors,
  competitors => competitors,
);

export const getSelectedCompetitorsIds = createSelector(
  selectedCompetitors,
  ids => ids,
);

export const getCompetitorsSelected = createSelector(
  getCompetitors,
  getSelectedCompetitorsIds,
  (competitors, ids) => competitors.map(competitor => {
    const found = _.find(ids, (id) =>  id === competitor.id);
    return {
      ...competitor,
      selected: found ? true : false,
    }
  })
);
